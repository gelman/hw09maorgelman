#include "BSNode.h"
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{
	/*check for part3 - printNodes*/

	BSNode* newBs = new BSNode("1");
	newBs->insert("1");
	newBs->insert("2");
	newBs->insert("2");
	newBs->insert("3");
	newBs->insert("3");
	newBs->insert("4");
	newBs->insert("4");
	newBs->insert("5");
	newBs->insert("5");

	newBs->printNodes(); /*printing as requested*/

	cout << endl;
	cout << endl;


	/*checking all the methods*/

	/*building new tree*/
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");

	/*printing the tree before the changes*/
	cout << "Printing the nodes: " << endl;
	bs->printNodes();

	cout << endl;
	cout << endl;

	/*printing the tree height and depth in different places*/
	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 6 depth: " << bs->getDepth(*(bs->getLeft())) << endl; /*error*/

	cout << endl;

	/*checking the search function*/
	cout << "9 in the tree? " << bs->search("9") << endl;
	cout << "5 in the tree? " << bs->search("5") << endl;
	cout << "50 in the tree? " << bs->search("50") << endl;
	cout << "2 in the tree? " << bs->search("2") << endl;
	cout << "11 in the tree? " << bs->search("11") << endl;
	cout << "6 in the tree? " << bs->search("6") << endl;

	cout << endl;

	cout << "Inserting new nodes" << endl;
	cout << endl;

	/*addingg nodes to the tree*/
	bs->insert("f");
	bs->insert("1");
	bs->insert("0");
	bs->insert("y");
	bs->insert("e");
	bs->insert("z");

	/*checking if the height changed correctly*/
	cout << "New tree height: " << bs->getHeight() << endl;

	cout << endl;

	cout << "Printing the nodes: " << endl;
	bs->printNodes(); /*printing the tree after the changes*/

	cout << endl;
	cout << endl;

	cout << endl;
	cout << endl;

	/*deleting nodes*/
	cout << "deleting nodes" << endl << endl;
	bs = bs->deleteNode(bs, "z");
	bs = bs->deleteNode(bs, "5");

	/*printing tree after the deletes*/
	cout << "printing tree after the deletes:" << endl << endl;
	bs->printNodes(); /*printing the nodes*/

	/*inserting the nodes again*/
	bs->insert("5"); /*this value will be in another place now
					 */
	bs->insert("z");

	cout << endl;
	cout << endl;

	/*checking the copy constructor*/
	BSNode bs2(*bs);
	cout << "bs2 Tree height: " << bs2.getHeight() << " - like the origin tree" << endl; /*same height as bs*/

	cout << endl;
	cout << endl;

	/*checking the operator = */
	BSNode bs3 = *bs;
	cout << "bs3 Tree height: " << bs3.getHeight() << " - like the origin tree" << endl; /*same height as bs*/

	/*writing to the file*/
	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	/*running the exe*/
	system("BinaryTree.exe");

	system("pause");
	remove(textTree.c_str());

	delete bs; /*free the memory*/

	return 0;
}