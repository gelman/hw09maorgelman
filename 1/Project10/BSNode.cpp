#include "BSNode.h"


/*constructor*/
BSNode::BSNode(string data)
{
	/*putting values in all the members*/

	this->_data = data;

	this->_right = nullptr;
	this->_left = nullptr;

	this->_count = 1; /*for part 2*/
}

/*copy constructor*/
BSNode::BSNode(const BSNode& other) : BSNode(other._data)
{
	/*copying right side*/
	if (other._right != nullptr)
	{
		this->_right = new BSNode(*other._right);
	}

	/*copying left side*/
	if (other._left != nullptr)
	{
		this->_left = new BSNode(*other._left);
	}

}

/*destructor*/
BSNode::~BSNode()
{

	/*deleting right side*/
	if (this->_right != nullptr)
	{
		delete this->_right;
	}

	/*deleting left side*/
	if (this->_left != nullptr)
	{
		delete this->_left;
	}

}

/*The function inserts new node to the tree*/
void BSNode::insert(string value)
{
	/*the data is already exist*/
	if (value == this->_data)
	{
		this->_count++;
	}
	else if (value.compare(this->_data) > 0) /*the data is bigger - going to the right side*/
	{
		if (this->_right == nullptr) /*there is no right side yet*/
		{
			this->_right = new BSNode(value); /*new node*/
		}
		else
		{
			this->_right->insert(value); /*the right side exist - going lower*/
		}
	}
	else /*the data is smaller - going to the left side*/
	{
		if (this->_left == nullptr) /*there is no left side yet*/
		{
			this->_left = new BSNode(value); /*new node*/
		}
		else
		{
			this->_left->insert(value); /*the left side exist - going lower*/
		}
	}
}

/*The function operates like the = operator*/
BSNode& BSNode::operator=(const BSNode& other)
{
	/*vopying the data*/
	this->_data = other._data;

	BSNode* prevRight = this->_right; /*saving previous node*/
	this->_right = new BSNode(*other._right); /*new node*/

	delete prevRight; /*deleting the old node*/


	BSNode* prevLeft = this->_left; /*saving previous node*/
	this->_left = new BSNode(*other._left); /*new node*/

	delete prevLeft; /*deleting the old node*/

	return *this; /*returning pointer to the new object*/
}

bool BSNode::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr); /*checking if the node has no children*/
}

/*gettres*/

string BSNode::getData() const
{
	return this->_data;
}

BSNode * BSNode::getLeft() const
{
	return this->_left;
}

BSNode * BSNode::getRight() const
{
	return this->_right;
}


/*The function search the tree for value and return true if it exist and false otherwise*/
bool BSNode::search(string val) const
{
	/*variables definition*/
	BSNode* currNode = nullptr;
	BSNode* rightNode = this->getRight();
	BSNode* leftNode = this->getLeft();
	string currData = this->getData();

	/*the data exist or not*/
	bool found = false;


	if (!(currData.compare(val))) /*checking if the data found*/
	{
		found = true;
	}
	else if (currData.compare(val) < 0) /*the current data is smaller then the val*/
	{
		currNode = rightNode; /*going to the right side*/
	}
	else /*the current data is bigger then the val*/
	{
		currNode = leftNode; /*going to the left side*/
	}
	while (currNode != nullptr && !found)
	{
		if (currNode->_data.compare(val) < 0) /*checking if the val bigger*/
		{
			currNode = currNode->_right; /*right side*/
		}
		else if (!(currNode->_data.compare(val))) /*checking if the val found*/
		{
			found = true;
		}
		else
		{
			currNode = currNode->_left; /*left side*/
		}
	}

	return found; /*returning the outcome of the search*/
}

/*The function calculates the height of the tree(could be sub tree)*/
int BSNode::getHeight() const
{
	/*calculating the max height in every side*/
	int rightMax = calculateHeight(this->_right);
	int leftMax = calculateHeight(this->_left);

	/*taking the bigger one*/

	if (rightMax > leftMax)
	{
		return rightMax; /*taking the right side*/
	}
	else
	{
		return leftMax; /*taking the left side*/
	}
}

/*The function helps to calculate the height*/
int BSNode::calculateHeight(const BSNode* root) const
{
	/*variables definition*/
	int right = 0, left = 0;

	if (root == nullptr) /*we came to the end*/
	{
		return 0;
	}
	else
	{
		/*calculating the height of every side*/
		right = calculateHeight(root->_right);
		left = calculateHeight(root->_left);

		/*taking the bigeer one*/
		if (right > left)
		{
			return (right + 1);
		}
		else
		{
			return (left + 1);
		}
	}
}

/*The function calculates the depth*/
int BSNode::getDepth(const BSNode & root) const
{
	if (!(root.search(this->_data))) /*checking if this object is child of the node*/
	{
		return ERROR; /*this object isn't a child of the node*/
	}
	/*calcaulating the dist*/
	return this->getCurrNodeDistFromInputNode(&root);
}

/*The function helps to calculate the depth*/
int BSNode::getCurrNodeDistFromInputNode(const BSNode * node) const
{
	/*variables definition*/
	int depth = 0;

	if (!(node->_data.compare(this->_data))) /*we found the data we needed*/
	{
		return depth;
	}
	else if (node->_left != nullptr) /*checking if we can go left*/
	{
		depth = getCurrNodeDistFromInputNode(node->_left) + 1; /*going left*/
	}
	else if (node->_right != nullptr) /*checking if we can go right*/
	{
		depth = getCurrNodeDistFromInputNode(node->_right) + 1; /*going right*/
	}

	return depth; /*returning the outcome*/
}




/*The function prints the nodes as requested*/
void BSNode::printNodes() const
{

	if (this->_left != nullptr)
	{
		this->_left->printNodes(); /*going left*/
	}

	cout << "data: " << _data << "  " << "times: " << _count << endl; /*printing the data and the times it entered*/

	if (this->_right != nullptr)
	{
		this->_right->printNodes(); /*going right*/
	}

}

/*The function deletes node according to the sended value*/
BSNode* BSNode::deleteNode(BSNode* root, string val)
{
	
	if (root == nullptr) /*checking if the tree exist*/
	{
		return root;
	}
	else if (root->_data.compare(val) > 0) /*checking if the value is smaller then the current value of the node*/
	{
		root->_left = deleteNode(root->_left, val); /*going left*/
	}
	else if (root->_data.compare(val) < 0) /*checking if the value is bigger then the current value of the node*/
	{
		root->_right = deleteNode(root->_right, val); /*going right*/
	}
	else
	{
		if (root->_left == nullptr) /*checking if there isn't more left nodes*/
		{
			BSNode* p = root->_right; /*saving the right side*/

			delete root; /*deleting memory*/
			return p;
		}
		else if (root->_right == nullptr) /*checking if there isn't more right nodes*/
		{
			BSNode* p = root->_left; /*saving the left side*/

			delete root; /*deleting memory*/
			return p;
		}
		
		/*getting the min value in the tree*/
		BSNode* p = minValueNode(root->_right);

		/*putting the min value data in the root as needed if we delete root(could be a root of a sub tree)*/
		root->_data = p->_data;

		/*calling the function again and putting the outcome in the right side of the root as needed if we delete root*/
		root->_right = deleteNode(root->_right, p->_data);
	}
	return root;
}

/*The function finds the smallest value in the tree*/
BSNode* BSNode::minValueNode(BSNode* node)
{
	/*variables definition*/
	BSNode* checkNode = node;

	/*running untill we got to the smallest value in the tree*/
	while (checkNode->getLeft() != nullptr)
	{
		checkNode = checkNode->getLeft(); /*moving to the next left side*/
	}
	return checkNode; /*returning the node that contains the min value*/
}