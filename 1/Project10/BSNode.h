#ifndef BSNode_H
#define BSNode_H

#include <string>
#include <algorithm> 
#include <iostream>
#define ERROR -1
using namespace std;

/*class definition*/

class BSNode
{
public:
	/*methods*/
	BSNode(string data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;

	/*getters*/
	string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(string val) const;

	/*calculations*/
	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

	BSNode* deleteNode(BSNode* root, string val);
	BSNode* minValueNode(BSNode* node);

private:
	/*members*/
	string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

	/*auxiliary function for getHeight*/
	int calculateHeight(const BSNode* root) const;
};

#endif