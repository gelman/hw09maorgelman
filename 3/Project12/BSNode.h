#ifndef BSNode_H
#define BSNode_H

#include <string>
#include <algorithm> 
#include <iostream>
#define ERROR -1
using namespace std;

/*class definition*/

template <class T>
class BSNode
{
public:
	/*methods*/
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;

	/*getters*/
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	/*calculations*/
	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	/*members*/
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

																/*auxiliary function for getHeight*/
	int calculateHeight(const BSNode* root) const;
};

#endif


/*constructor*/
template <class T>
BSNode<T>::BSNode(T data)
{
	/*putting values in all the members*/

	this->_data = data;

	this->_right = nullptr;
	this->_left = nullptr;

	this->_count = 1; /*for part 2*/
}

/*copy constructor*/
template <class T>
BSNode<T>::BSNode(const BSNode& other) : BSNode(other._data)
{
	/*copying right side*/
	if (other._right != nullptr)
	{
		this->_right = new BSNode(*other._right);
	}

	/*copying left side*/
	if (other._left != nullptr)
	{
		this->_left = new BSNode(*other._left);
	}

}

/*destructor*/
template <class T>
BSNode<T>::~BSNode()
{

	/*deleting right side*/
	if (this->_right != nullptr)
	{
		delete this->_right;
	}

	/*deleting left side*/
	if (this->_left != nullptr)
	{
		delete this->_left;
	}

}

/*The function inserts new node to the tree*/
template <class T>
void BSNode<T>::insert(T value)
{
	/*the data is already exist*/
	if (value == this->_data)
	{
		this->_count++;
	}
	else if (value > this->_data) /*the data is bigger - going to the right side*/
	{
		if (this->_right == nullptr) /*there is no right side yet*/
		{
			this->_right = new BSNode(value); /*new node*/
		}
		else
		{
			this->_right->insert(value); /*the right side exist - going lower*/
		}
	}
	else /*the data is smaller - going to the left side*/
	{
		if (this->_left == nullptr) /*there is no left side yet*/
		{
			this->_left = new BSNode(value); /*new node*/
		}
		else
		{
			this->_left->insert(value); /*the left side exist - going lower*/
		}
	}
}

/*The function operates like the = operator*/
template <class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	/*vopying the data*/
	this->_data = other._data;

	BSNode* prevRight = this->_right; /*saving previous node*/
	this->_right = new BSNode(*other._right); /*new node*/

	delete prevRight; /*deleting the old node*/


	BSNode* prevLeft = this->_left; /*saving previous node*/
	this->_left = new BSNode(*other._left); /*new node*/

	delete prevLeft; /*deleting the old node*/

	return *this; /*returning pointer to the new object*/
}

template <class T>
bool BSNode<T>::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr); /*checking if the node has no children*/
}

/*gettres*/
template <class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template <class T>
BSNode<T> * BSNode<T>::getLeft() const
{
	return this->_left;
}

template <class T>
BSNode<T> * BSNode<T>::getRight() const
{
	return this->_right;
}


/*The function search the tree for value and return true if it exist and false otherwise*/
template <class T>
bool BSNode<T>::search(T val) const
{
	/*variables definition*/
	BSNode* currNode = nullptr;
	BSNode* rightNode = this->getRight();
	BSNode* leftNode = this->getLeft();
	string currData = this->getData();

	/*the data exist or not*/
	bool found = false;


	if (!(currData.compare(val))) /*checking if the data found*/
	{
		found = true;
	}
	else if (currData.compare(val) < 0) /*the current data is smaller then the val*/
	{
		currNode = rightNode; /*going to the right side*/
	}
	else /*the current data is bigger then the val*/
	{
		currNode = leftNode; /*going to the left side*/
	}
	while (currNode != nullptr && !found)
	{
		if (currNode->_data.compare(val) < 0) /*checking if the val bigger*/
		{
			currNode = currNode->_right; /*right side*/
		}
		else if (!(currNode->_data.compare(val))) /*checking if the val found*/
		{
			found = true;
		}
		else
		{
			currNode = currNode->_left; /*left side*/
		}
	}

	return found; /*returning the outcome of the search*/
}

/*The function calculates the height of the tree(could be sub tree)*/
template <class T>
int BSNode<T>::getHeight() const
{
	/*calculating the max height in every side*/
	int rightMax = calculateHeight(this->_right);
	int leftMax = calculateHeight(this->_left);

	/*taking the bigger one*/

	if (rightMax > leftMax)
	{
		return rightMax; /*taking the right side*/
	}
	else
	{
		return leftMax; /*taking the left side*/
	}
}

/*The function helps to calculate the height*/
template <class T>
int BSNode<T>::calculateHeight(const BSNode* root) const
{
	/*variables definition*/
	int right = 0, left = 0;

	if (root == nullptr) /*we came to the end*/
	{
		return 0;
	}
	else
	{
		/*calculating the height of every side*/
		right = calculateHeight(root->_right);
		left = calculateHeight(root->_left);

		/*taking the bigeer one*/
		if (right > left)
		{
			return (right + 1);
		}
		else
		{
			return (left + 1);
		}
	}
}

/*The function calculates the depth*/
template <class T>
int BSNode<T>::getDepth(const BSNode & root) const
{
	if (!(root.search(this->_data))) /*checking if this object is child of the node*/
	{
		return ERROR; /*this object isn't a child of the node*/
	}
	/*calcaulating the dist*/
	return this->getCurrNodeDistFromInputNode(&root);
}

/*The function helps to calculate the depth*/
template <class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode * node) const
{
	/*variables definition*/
	int depth = 0;

	if (!(node->_data.compare(this->_data))) /*we found the data we needed*/
	{
		return depth;
	}
	else if (node->_left != nullptr) /*checking if we can go left*/
	{
		depth = getCurrNodeDistFromInputNode(node->_left) + 1; /*going left*/
	}
	else if (node->_right != nullptr) /*checking if we can go right*/
	{
		depth = getCurrNodeDistFromInputNode(node->_right) + 1; /*going right*/
	}

	return depth; /*returning the outcome*/
}




/*The function prints the nodes as requested*/
template <class T>
void BSNode<T>::printNodes() const
{

	if (this->_left != nullptr)
	{
		this->_left->printNodes(); /*going left*/
	}

	cout << "data: " << _data << "  " << "times: " << _count << endl; /*printing the data and the times it entered*/

	if (this->_right != nullptr)
	{
		this->_right->printNodes(); /*going right*/
	}

}





