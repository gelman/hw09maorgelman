#include "BSNode.h"

#define ARR_SIZE 15

int main()
{
	/*arrays definition*/
	int arrayOfIntegers[ARR_SIZE] = { 100, 23, 6, 1, 5, 89, 90, 234, 10, 83, 4, 80, 765, 91, 72 };
	string arrayOfStrings[ARR_SIZE] = { "b", "c", "e", "w", "z", "q", "a", "d", "f", "p", "m", "t", "y", "g", "k" };

	/*creating new roots for two new trees*/
	BSNode<string>* stringsBs = new BSNode<string>(arrayOfStrings[0]);
	BSNode<int>* integersBs = new BSNode<int>(arrayOfIntegers[0]);

	/*running over all the array*/
	for (int i = 0; i < ARR_SIZE; i++)
	{
		cout << arrayOfStrings[i] << " "; /*printing all the values*/

		stringsBs->insert(arrayOfStrings[i]); /*inserting all the values to the tree*/
	}

	cout << endl;
	cout << endl;

	/*running over all the array*/
	for (int i = 0; i < ARR_SIZE; i++)
	{
		cout << arrayOfIntegers[i] << " "; /*printing all the values*/

		integersBs->insert(arrayOfIntegers[i]); /*inserting all the values to the tree*/
	}
	
	cout << endl;
	cout << endl;

	/*printing all the nodes*/
	stringsBs->printNodes();

	cout << endl;
	cout << endl;

	/*printing all the nodes*/
	integersBs->printNodes();

	system("pause");

	return  0;
}