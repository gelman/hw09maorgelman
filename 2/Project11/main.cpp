#include "functions.h"



int main() 
{

	/*magshimim's part*/
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++)
	{
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	/*my part*/
	/*checking compare - char*/
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'f') << std::endl;
	std::cout << compare<char>('g', 'a') << std::endl;
	std::cout << compare<char>('r', 'r') << std::endl;

	/*checking bubbleSort - char*/
	std::cout << "correct print is sorted array" << std::endl;

	/*variables definition*/
	const int arraySizeChar = 6;
	char charArray[arraySizeChar] = { 'd', 'v', 'z', 'a', 'b', 'w'};

	bubbleSort<char>(charArray, arraySizeChar); /*sorting the array*/

	/*printing the array after the sort*/
	for (int i = 0; i < arraySizeChar; i++)
	{
		std::cout << charArray[i] << " ";
	}

	std::cout << std::endl;
	std::cout << std::endl;

	/*checking printArray - char*/
	std::cout << "correct print is sorted array" << std::endl;

	printArray<char>(charArray, arraySizeChar); /*printing the array with the printArray function*/

	std::cout << std::endl;
	std::cout << std::endl;

	
	/*new objects*/
	myObject fisrtObject(1);
	myObject secondObject(2);
	myObject thirdObject(3);
	myObject fourthObject(4);
	myObject fifthObject(5);
	myObject sixthObject(6);

	/*checking compare - myObject*/
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<myObject>(fisrtObject, thirdObject) << std::endl;
	std::cout << compare<myObject>(sixthObject, secondObject) << std::endl;
	std::cout << compare<myObject>(fourthObject, fourthObject) << std::endl;

	/*checking bubbleSort - myObject*/
	std::cout << "correct print is sorted array" << std::endl;

	/*variables definition*/
	const int arraySizeMyObject = 6;
	myObject myObjectArray[arraySizeMyObject] = { thirdObject, fisrtObject, sixthObject, secondObject, fifthObject,  fourthObject };

	bubbleSort<myObject>(myObjectArray, arraySizeMyObject); /*sotring the array of objects*/

	/*printing the array*/
	for (int i = 0; i < arraySizeMyObject; i++)
	{
		std::cout << myObjectArray[i];
	}

	std::cout << std::endl;
	std::cout << std::endl;

	/*checking printArray - myObject*/
	std::cout << "correct print is sorted array" << std::endl;
	printArray<myObject>(myObjectArray, arraySizeMyObject); /*printing the array with the printArray function*/

	
	system("pause");

	return 0;
}