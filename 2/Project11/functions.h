#pragma once
#include <iostream>

/*class definition*/
class myObject
{
public:
	/*methods*/
	myObject(int data);
	~myObject();

	/*operators*/
	bool operator== (const myObject& object);
	bool operator< (const myObject& object);
	bool operator> (const myObject& object);

	/*ostream operator*/
	friend std::ostream &operator<<(std::ostream &output, const myObject &object);

private:
	/*members*/
	int _data;

};


template <class T>
void swap(T& n1, T& n2);

template <class T>
int compare(T n1, T n2);


template <class T>
void printArray(T* array, const int size);

template <class T>
void bubbleSort(T* array, const int  size);


/*The function swap two elements*/
template<class T>
void swap(T & n1, T & n2)
{
	T temp = n1;
	n1 = n2;
	n2 = temp;
}

/*The function compares two elements*/
template<class T>
int compare(T n1, T n2)
{
	if (n1 > n2)
	{
		return -1; /*n1 bigger*/
	}
	else if (n1 < n2)
	{
		return 1; /*n2 smaller*/
	}
	else
	{
		return 0; /*equal values*/
	}
}


/*The function prints the array of elemets*/
template<class T>
void printArray(T * array, const int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << array[i] << std::endl; /*printing the values*/
	}
}


/*The function sorts array of elements*/
template<class T>
void bubbleSort(T * array, const int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (array[j] > array[j + 1]) /*checking if we need to swap*/
			{
				swap(array[j], array[j + 1]); /*swaing values*/
			}
		}
	}
}





