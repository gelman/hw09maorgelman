#include "functions.h"

/*constructor*/
myObject::myObject(int data)
{
	this->_data = data; /*setting data*/
}

/*destructor*/
myObject::~myObject()
{
	/*no need to free memory*/
}


/* > operator */
bool myObject::operator>(const myObject & object)
{
	return (this->_data > object._data); /*checking if bigger*/
}

/* == operator */
bool myObject::operator==(const myObject& object)
{
	return (this->_data == object._data); /*checking if equal*/
}

/* < operator */
bool myObject::operator<(const myObject & object) 
{
	return (this->_data < object._data); /*checking if smaller*/
}

/* ostream operator*/
std::ostream & operator<<(std::ostream & output, const myObject& object)
{
	std::cout << object._data; /*printing the object data*/
	return output;
}